export interface ICompany {
  id?: number;
  name?: string;
  address?: string;
  city?: string;
  businessSector?: string;
  email?: string;
  lang?: string;
  password?: string;
  createdAt?: Date;
  updatedAt?: Date;
}
