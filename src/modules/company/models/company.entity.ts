import { DepartmentEntity } from 'src/modules/department/models/department.entity';
import { EmployeeEntity } from 'src/modules/employee/models/employee.entity';
import { GenericEntity } from 'src/shared/entities/generic.entity';
import { BeforeInsert, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'companies' })
export class CompanyEntity extends GenericEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  name: string;

  @Column({ nullable: true })
  address: string;

  @Column({ nullable: true })
  city: string;

  @Column({ nullable: false, default: 'fr' })
  lang: string;

  @Column({ nullable: true })
  businessSector: string;

  @Column({ unique: true })
  email: string;

  @Column({ select: false })
  password: string;

  @OneToMany(() => EmployeeEntity, (employee: EmployeeEntity) => employee.company)
  employees: EmployeeEntity[];

  @OneToMany(() => DepartmentEntity, (department: DepartmentEntity) => department.company)
  departments: DepartmentEntity[];

  @BeforeInsert()
  emailToLowerCase(): void {
    this.email = this.email.toLowerCase();
  }
}
