import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { catchError, from, map, Observable, of, switchMap, throwError } from 'rxjs';
import { FindOneOptions, Repository } from 'typeorm';
import { AuthService } from '../auth/auth.service';
import { CompanyEntity } from './models/company.entity';
import { ICompany } from './models/company.interface';

@Injectable()
export class CompanyService {
  constructor(
    @InjectRepository(CompanyEntity) private readonly companyRepository: Repository<CompanyEntity>,
    private authService: AuthService
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Creates a new Company entity if it does not already exist in database.
   *
   * @param     { ICompany }      company
   * @returns   { Observable<ICompany> }
   */
  create(company: ICompany): Observable<ICompany> {
    return this.authService.hashPassword(company.password).pipe(
      switchMap((hash: string) => {
        const newCompany = new CompanyEntity();

        newCompany.name = company.name;
        newCompany.email = company.email;
        newCompany.lang = company.lang;
        newCompany.password = hash;

        return from(this.companyRepository.save(newCompany)).pipe(
          map((company: ICompany) => this.getCompanyWithoutPassword(company)),
          catchError((error) => throwError(() => new BadRequestException('CREDENTIALS_ALREADY_EXIST')))
        );
      })
    );
  }

  /**
   * Returns a JSON Web Token if the company is properly authenticated.
   *
   * @param     { ICompany }      company
   * @returns   { Observable<any> }
   */
  login(company: ICompany): Observable<string> {
    return this.validateCompanyCredentials(company.email, company.password).pipe(
      switchMap((validatedCompany: ICompany) => {
        return validatedCompany
          ? this.authService.generateJwtToken(validatedCompany).pipe(map((token: string) => token))
          : throwError(() => new BadRequestException('BAD_CREDENTIALS'));
      })
    );
  }

  /**
   * Returns one Company entity with the corresponding id.
   *
   * @param     { number }      id
   * @returns   { Observable<ICompany> }
   */
  findOne(id: number): Observable<ICompany> {
    const options: FindOneOptions = { where: { id } };

    return from(this.companyRepository.findOne(options)).pipe(
      switchMap((company: ICompany) => of(this.getCompanyWithoutPassword(company))),
      catchError(() => throwError(() => new NotFoundException('USER_NOT_FOUND')))
    );
  }

  /**
   * Returns one Company entity with the corresponding email.
   *
   * @param     { number }      email
   * @returns   { Observable<ICompany> }
   */
  findOneByMail(email: string): Observable<ICompany> {
    return from(this.companyRepository.findOne({ where: { email } })).pipe(
      switchMap((company: ICompany) => of(this.getCompanyWithoutPassword(company))),
      catchError(() => throwError(() => new NotFoundException('USER_NOT_FOUND')))
    );
  }

  /**
   * Deletes one Company entity with the corresponding id.
   *
   * @param     { number }      id
   * @returns   { Observable<any> }
   */
  deleteOne(id: number): Observable<any> {
    return from(this.companyRepository.delete(id)).pipe(
      catchError(() => throwError(() => new BadRequestException('USER_CANNOT_BE_DELETED')))
    );
  }

  /**
   * Updates one Company entity with the corresponding id.
   *
   * @param     { number }      id
   * @param     { ICompany }    company
   * @returns   { Observable<any> }
   */
  updateOne(id: number, company: ICompany): Observable<ICompany> {
    const { email, password, ...partialCompany } = company;
    return from(this.companyRepository.update(id, partialCompany)).pipe(
      switchMap(() => this.findOne(id)),
      catchError((error: Error) => throwError(() => error))
    );
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Returns a ICompany item if :
   * - A company with the corresponding email is found.
   * - If the company's hashed password matches with the password parameter.
   *
   * @param     { string }      email
   * @param     { string }      password
   * @returns   { Observable<ICompany> }
   */
  private validateCompanyCredentials(email: string, password: string): Observable<ICompany> {
    return from(this.companyRepository.findOne({ where: { email }, select: ['id', 'email', 'password'] })).pipe(
      switchMap((company: ICompany) => {
        return company
          ? this.authService
              .comparePasswords(password, company.password)
              .pipe(map((match: boolean) => (match ? this.getCompanyWithoutPassword(company) : null)))
          : throwError(() => new BadRequestException('ACCOUNT_NOT_FOUND'));
      })
    );
  }

  /**
   * Returns a ICompany item excluding the company's password.
   *
   * @param     { ICompany }      company
   * @returns   { ICompany }
   */
  private getCompanyWithoutPassword(company: ICompany): ICompany {
    const { password, ...result } = company;
    return result;
  }
}
