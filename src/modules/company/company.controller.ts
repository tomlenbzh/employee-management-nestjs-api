import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { catchError, map, Observable, switchMap } from 'rxjs';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { CompanyService } from './company.service';
import { ICompany } from './models/company.interface';

@Controller('companies')
export class CompanyController {
  constructor(private companyService: CompanyService) {}

  /**
   * Inserts a new company in database if it doesn't already exist.
   *
   * @param     { ICompany }      company
   * @returns   { Observable<ICompany> }
   */
  @Post()
  create(@Body() company: ICompany): Observable<ICompany> {
    return this.companyService.create(company).pipe(
      map((company: ICompany) => company),
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  /**
   * Returns a JSON Web Token and the authenticated company information.
   *
   * @param     { ICompany }     company
   * @returns   { Observable<any> }
   */
  @Post('login')
  login(@Body() company: ICompany): Observable<any> {
    return this.companyService.login(company).pipe(
      switchMap((token: string) =>
        this.companyService.findOneByMail(company.email).pipe(map((res: ICompany) => ({ token, company: res })))
      ),
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  /**
   * Returns a single company based on its id.
   *
   * @param     { string }      id
   * @returns   { Observable<ICompany> }
   */
  @Get(':id')
  @UseGuards(JwtAuthGuard)
  findOne(@Param('id') id: string): Observable<ICompany> {
    return this.companyService.findOne(Number(id));
  }

  /**
   * Updates one company's information.
   *
   * @param     { string }      id
   * @param     { string }      company
   * @returns   { Observable<ICompany> }
   */
  @Put(':id')
  @UseGuards(JwtAuthGuard)
  updateOne(@Param('id') id: string, @Body() company: ICompany): Observable<ICompany> {
    return this.companyService.updateOne(Number(id), company).pipe(
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  /**
   * Deletes a company and all related informations.
   *
   * @param     { string }      id
   * @returns   { Observable<any> }
   */
  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  deleteOne(@Param('id') id: string): Observable<any> {
    return this.companyService.deleteOne(Number(id));
  }
}
