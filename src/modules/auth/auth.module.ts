import { forwardRef, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { JwtAuthGuard } from './guards/jwt.guard';
import { AuthService } from './auth.service';
import { JwtStrategy } from './guards/jwt.strategy';
import { TOKEN_EXPIRES_IN } from 'src/shared/constants/token.constant';
import { CompanyModule } from '../company/company.module';

@Module({
  imports: [
    forwardRef(() => CompanyModule),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => ({
        secret: config.get('JWT_SECRET'),
        signOptions: { expiresIn: TOKEN_EXPIRES_IN }
      })
    })
  ],
  providers: [AuthService, JwtStrategy, JwtAuthGuard],
  exports: [AuthService]
})
export class AuthModule {}
