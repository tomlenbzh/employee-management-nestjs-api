import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { from, Observable } from 'rxjs';
import * as bcrypt from 'bcrypt';
import { ICompany } from '../company/models/company.interface';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  /**
   * Generates and returns a new JSON Web Token.
   *
   * @param     { ICompany }      company
   * @returns   { Observable<string> }
   */
  generateJwtToken(company: ICompany): Observable<string> {
    return from(this.jwtService.signAsync({ company })); // TEST USE USER ??
  }

  /**
   * Returns a hashed password.
   *
   * @param     { string }      password
   * @returns   { Observable<string> }
   */
  hashPassword(password: string): Observable<any> {
    return from<any>(bcrypt.hash(password, 12));
  }

  /**
   * Returns whether or not the given password matches with the stored hashed password.
   *
   * @param     { string }      newPassword
   * @param     { string }      passwortHash
   * @returns   { Observable<boolean> }
   */
  comparePasswords(newPassword: string, passwortHash: string): Observable<boolean> {
    return from(bcrypt.compare(newPassword, passwortHash) as Promise<boolean>);
  }
}
