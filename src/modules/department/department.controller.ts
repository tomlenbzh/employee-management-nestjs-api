import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { catchError, map, Observable } from 'rxjs';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { DepartmentService } from './department.service';
import { IDepartment } from './models/department.interface';

@Controller('departments')
export class DepartmentController {
  constructor(private departmentService: DepartmentService) {}

  /**
   * Inserts a new department in database if it doesn't already exist.
   *
   * @param     { IDepartment }      newDepartment
   * @returns   { Observable<IDepartment> }
   */
  @Post()
  @UseGuards(JwtAuthGuard)
  create(@Body() newDepartment: IDepartment): Observable<IDepartment> {
    return this.departmentService.create(newDepartment).pipe(
      map((department: IDepartment) => department),
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  /**
   * Returns a list of departments belonging to a certain company.
   *
   * @param       { ParseIntPipe }      companyId
   * @returns     { Observable<IDepartment[]> }
   */
  @Get('company/:company')
  @UseGuards(JwtAuthGuard)
  findAllByUser(@Param('company') companyId: number): Observable<IDepartment[]> {
    return this.departmentService.findAllByCompany(Number(companyId));
  }

  /**
   * Returns a department based on its id.
   *
   * @param       { string }      id
   * @returns     { Observable<IDepartment> }
   */
  @Get(':id')
  @UseGuards(JwtAuthGuard)
  findOne(@Param('id') id: string): Observable<IDepartment> {
    return this.departmentService.findOne(Number(id));
  }

  /**
   * Updates a single department based on its id.
   *
   * @param       { string }          id
   * @param       { IDepartment }     department
   * @returns     { Observable<IDepartment> }
   */
  @Put(':id')
  @UseGuards(JwtAuthGuard)
  updateOne(@Param('id') id: string, @Body() department: IDepartment): Observable<IDepartment> {
    return this.departmentService.updateOne(Number(id), department).pipe(
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  /**
   * Removes a department based on its id.
   *
   * @param       { string }      id
   * @returns     { Observable<any> }
   */
  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  delete(@Param('id') id: string): Observable<any> {
    return this.departmentService.deleteOne(Number(id));
  }
}
