import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { catchError, from, map, mergeMap, Observable, throwError } from 'rxjs';
import { Repository } from 'typeorm';
import { DepartmentEntity } from './models/department.entity';
import { IDepartment } from './models/department.interface';

@Injectable()
export class DepartmentService {
  constructor(@InjectRepository(DepartmentEntity) private readonly departmentRepository: Repository<DepartmentEntity>) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Inserts a new department in database if it doesn't already exist.
   *
   * @param     { IDepartment }      department
   * @returns   { Observable<IDepartment> }
   */
  create(department: IDepartment): Observable<IDepartment> {
    return from(this.departmentRepository.save(department, {})).pipe(
      map((createdDepartment: IDepartment) => createdDepartment),
      catchError(() => throwError(() => new BadRequestException('DEPARTMENT_COULD_NOT_BE_CREATED')))
    );
  }

  /**
   * Returns a non paginated list of departments belonging to a certain company.
   *
   * @param       { number }      id
   * @returns     { Observable<IDepartment[]> }
   */
  findAllByCompany(id: number): Observable<IDepartment[]> {
    return from(
      this.departmentRepository
        .createQueryBuilder('department')
        .leftJoinAndSelect('department.manager', 'manager')
        .loadRelationCountAndMap('department.employeesCount', 'department.employees')
        .where('department.company = :id', { id })
        .getMany()
    ).pipe(
      map((departments: IDepartment[]) => departments),
      catchError((error: Error) => throwError(() => new BadRequestException(error)))
    );
  }

  /**
   * Returns a department based on its id.
   *
   * @param       { number }      id
   * @returns     { Observable<IDepartment> }
   */
  findOne(id: number): Observable<IDepartment> {
    return from(
      this.departmentRepository
        .createQueryBuilder('department')
        .leftJoinAndSelect('department.manager', 'manager')
        .loadRelationCountAndMap('department.employeesCount', 'department.employees')
        .where('department.id = :id', { id })
        .getOne()
    ).pipe(
      map((department: IDepartment) => department),
      catchError((error: Error) => throwError(() => new BadRequestException(error)))
    );
  }

  /**
   * Updates a single department based on its id.
   *
   * @param       { number }          id
   * @param       { IDepartment }     department
   * @returns     { Observable<IDepartment> }
   */
  updateOne(id: number, department: IDepartment): Observable<IDepartment> {
    const { updatedAt, employeesCount, ...partialDepartment } = department;
    return from(this.departmentRepository.update(id, partialDepartment)).pipe(
      mergeMap(() => this.findOne(id)),
      catchError((error: Error) => throwError(() => new BadRequestException(error)))
    );
  }

  /**
   * Removes a project based on its id.
   *
   * @param       { number }      id
   * @returns     { Observable<any> }
   */
  deleteOne(id: number): Observable<any> {
    return from(this.departmentRepository.delete(id)).pipe(
      catchError(() => throwError(() => new BadRequestException('PROJECT_CANNOT_BE_DELETED')))
    );
  }
}
