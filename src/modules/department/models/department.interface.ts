import { ICompany } from 'src/modules/company/models/company.interface';
import { IEmployee } from 'src/modules/employee/models/employee.interface';

export interface IDepartment {
  id?: number;
  name?: string;
  description?: string;
  manager?: IEmployee;
  company?: ICompany;
  employees?: IEmployee[];
  employeesCount?: number;
  createdAt?: Date;
  updatedAt?: Date;
}
