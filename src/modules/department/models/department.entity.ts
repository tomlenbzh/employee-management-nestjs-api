import { CompanyEntity } from 'src/modules/company/models/company.entity';
import { EmployeeEntity } from 'src/modules/employee/models/employee.entity';
import { GenericEntity } from 'src/shared/entities/generic.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'departments' })
export class DepartmentEntity extends GenericEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  name: string;

  @Column({ nullable: true })
  description: string;

  @OneToOne(() => EmployeeEntity, { onUpdate: 'CASCADE', onDelete: 'CASCADE', nullable: true })
  @JoinColumn({ name: 'managerId' })
  manager: EmployeeEntity;

  @ManyToOne(() => CompanyEntity, (company: CompanyEntity) => company.departments, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
    nullable: false
  })
  @JoinColumn({ name: 'companyId' })
  company: CompanyEntity;

  @OneToMany(() => EmployeeEntity, (employee: EmployeeEntity) => employee.department, {
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL',
    nullable: true
  })
  employees: EmployeeEntity[];
}
