import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { catchError, from, map, mergeMap, Observable, throwError } from 'rxjs';
import { Repository } from 'typeorm';
import { EmployeeEntity } from './models/employee.entity';
import { IEmployee } from './models/employee.interface';

@Injectable()
export class EmployeeService {
  constructor(@InjectRepository(EmployeeEntity) private readonly employeeRepository: Repository<EmployeeEntity>) {}

  /**
   * Inserts a new employee entity in database if it doesn't already exist.
   *
   * @param     { IEmployee }      employee
   * @returns   { Observable<IEmployee> }
   */
  create(employee: IEmployee): Observable<IEmployee> {
    return from(this.employeeRepository.save(employee)).pipe(
      map((newEmployee: IEmployee) => newEmployee),
      catchError(() => throwError(() => new BadRequestException('EMPLOYEE_COULD_NOT_BE_CREATED')))
    );
  }

  /**
   * Returns a list of employees based on their department id.
   *
   * @param       { number }      id
   * @returns     { Observable<IEmployee[]> }
   */
  findAllByDepartment(id: number): Observable<IEmployee[]> {
    return from(this.employeeRepository.find({ where: { department: { id } } })).pipe(
      map((employees: IEmployee[]) => employees),
      catchError(() => throwError(() => new NotFoundException('EMPLOYEE_NOT_FOUND')))
    );
  }

  /**
   * Returns a list of employees based on their company id.
   *
   * @param       { number }      id
   * @returns     { Observable<IEmployee[]> }
   */
  findAllByCompany(id: number): Observable<IEmployee[]> {
    const relations: string[] = ['department'];
    return from(this.employeeRepository.find({ where: { company: { id } }, relations })).pipe(
      map((employees: IEmployee[]) => employees),
      catchError(() => throwError(() => new NotFoundException('EMPLOYEE_NOT_FOUND')))
    );
  }

  /**
   * Returns an employee based on its id.
   *
   * @param       { number }      id
   * @returns     { Observable<IEmployee> }
   */
  findOne(id: number): Observable<IEmployee> {
    const relations: string[] = ['department'];
    return from(this.employeeRepository.findOne({ where: { id }, relations })).pipe(
      map((employee: IEmployee) => employee),
      catchError(() => throwError(() => new NotFoundException('EMPLOYEE_NOT_FOUND')))
    );
  }

  /**
   * Updates a single employee based on its id.
   *
   * @param       { number }          id
   * @param       { IEmployee }       employee
   * @returns     { Observable<IEmployee> }
   */
  updateOne(id: number, employee: IEmployee): Observable<IEmployee> {
    const { updatedAt, ...partialEmployee } = employee;
    return from(this.employeeRepository.update(id, partialEmployee)).pipe(
      mergeMap(() => this.findOne(id)),
      catchError((error: Error) => throwError(() => new BadRequestException(error)))
    );
  }

  /**
   * Removes an employee on its id.
   *
   * @param       { number }      id
   * @returns     { Observable<any> }
   */
  deleteOne(id: number): Observable<any> {
    return from(this.employeeRepository.delete(id)).pipe(
      catchError(() => throwError(() => new BadRequestException('EMPLOYEE_CANNOT_BE_DELETED')))
    );
  }
}
