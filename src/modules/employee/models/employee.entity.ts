import { CompanyEntity } from 'src/modules/company/models/company.entity';
import { DepartmentEntity } from 'src/modules/department/models/department.entity';
import { GenericEntity } from 'src/shared/entities/generic.entity';
import { BeforeInsert, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'employees' })
export class EmployeeEntity extends GenericEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  firstName: string;

  @Column({ nullable: false })
  lastName: string;

  @Column({ nullable: false })
  title: string;

  @Column({ nullable: false })
  email: string;

  @Column({ nullable: true })
  salary: number;

  @Column({ nullable: true })
  address: string;

  @Column({ nullable: true })
  city: string;

  @ManyToOne(() => CompanyEntity, (company: CompanyEntity) => company.employees, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
    nullable: false
  })
  @JoinColumn({ name: 'companyId' })
  company: CompanyEntity;

  @ManyToOne(() => DepartmentEntity, (department: DepartmentEntity) => department.employees, {
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL',
    nullable: true
  })
  @JoinColumn({ name: 'departmentId' })
  department: DepartmentEntity;

  @BeforeInsert()
  emailToLowerCase(): void {
    this.email = this.email.toLowerCase();
  }
}
