import { ICompany } from 'src/modules/company/models/company.interface';
import { IDepartment } from 'src/modules/department/models/department.interface';

export interface IEmployee {
  id?: number;
  firstName?: string;
  lastName?: string;
  title?: string;
  address?: string;
  city?: string;
  email?: string;
  salary?: number;
  company?: ICompany;
  department?: IDepartment;
  createdAt?: Date;
  updatedAt?: Date;
}
