import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { catchError, map, Observable } from 'rxjs';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { EmployeeService } from './employee.service';
import { IEmployee } from './models/employee.interface';

@Controller('employees')
export class EmployeeController {
  constructor(private employeeService: EmployeeService) {}

  /**
   * Inserts a new employee entity in database if it doesn't already exist.
   *
   * @param     { IEmployee }      newEmployee
   * @returns   { Observable<IEmployee> }
   */
  @Post()
  @UseGuards(JwtAuthGuard)
  create(@Body() newEmployee: IEmployee): Observable<IEmployee> {
    return this.employeeService.create(newEmployee).pipe(
      map((employee: IEmployee) => employee),
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  /**
   * Returns a list of employees based on their department id.
   *
   * @param       { string }      departmentId
   * @returns     { Observable<IEmployee> }
   */
  @Get('department/:department')
  @UseGuards(JwtAuthGuard)
  findByProject(@Param('department') departmentId: number): Observable<IEmployee[]> {
    return this.employeeService.findAllByDepartment(Number(departmentId));
  }

  /**
   * Returns a list of employees based on their department id.
   *
   * @param       { string }      companyId
   * @returns     { Observable<IEmployee> }
   */
  @Get('company/:company')
  @UseGuards(JwtAuthGuard)
  findAllByCompany(@Param('company') companyId: number): Observable<IEmployee[]> {
    return this.employeeService.findAllByCompany(Number(companyId));
  }

  /**
   * Returns an employee based on its id.
   *
   * @param       { string }      id
   * @returns     { Observable<IEmployee> }
   */
  @Get(':id')
  @UseGuards(JwtAuthGuard)
  findOne(@Param('id') id: string): Observable<IEmployee> {
    return this.employeeService.findOne(Number(id));
  }

  /**
   * Updates a single employee based on its id.
   *
   * @param       { string }           id
   * @param       { IEmployee }        employee
   * @returns     { Observable<IEmployee> }
   */
  @Put(':id')
  @UseGuards(JwtAuthGuard)
  updateOne(@Param('id') id: string, @Body() employee: IEmployee): Observable<IEmployee> {
    return this.employeeService.updateOne(Number(id), employee).pipe(
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  /**
   * Removes a employee based on its id.
   *
   * @param       { string }      id
   * @returns     { Observable<IEmployee> }
   */
  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  delete(@Param('id') id: string): Observable<IEmployee> {
    return this.employeeService.deleteOne(Number(id));
  }
}
